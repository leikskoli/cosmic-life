extends Node
class_name Room

@onready var mouse_box = $MouseBox
@onready var rig = $Rig

var mouse_down_position: Vector2
var mouse_down_rotation: Vector3
var mouse_multiplier = 0.08

func _physics_process(delta):
	if Input.is_action_just_pressed("orbit"):
		mouse_down_position = get_viewport().get_mouse_position()
		mouse_down_rotation = rig.rotation_degrees
	
	if Input.is_action_pressed("orbit"):
		rig.rotation_degrees.x =  clamp(mouse_down_rotation.x + (mouse_multiplier * (mouse_down_position.y - get_viewport().get_mouse_position().y)), -80, -10)
		rig.global_rotation_degrees.y = clamp(mouse_down_rotation.y + (mouse_multiplier * (mouse_down_position.x - get_viewport().get_mouse_position().x)), 10, 80)
