extends CharacterBody3D
class_name Furniture

@export var room: Room

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var is_dragging: bool = false
var move_direction: Vector3 = Vector3.ZERO
var move_speed = 5.0 # m / sec.


#func OnInputEvent(_camera, event, position, _normal, _shape_idx):
#	if event is InputEventMouseMotion:
		


func _ready():
	input_event.connect(OnPressed)
	room.mouse_box.input_event.connect(OnMouseDrag)


func OnPressed(_camera, event, position, _normal, _shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			is_dragging = true


func OnMouseDrag(_camera, event, position, _normal, _shape_idx):
	print("Dragging on mouse plane.")


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	if Input.is_action_just_released("drop_furniture"):
		is_dragging = false
	
	if is_dragging:
		#var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
		#move_direction.x = position.direction_to()
		var direction = (transform.basis * Vector3(move_direction.x, 0, move_direction.z)).normalized()
		if direction:
			velocity.x = direction.x * move_speed
			velocity.z = direction.z * move_speed
		else:
			velocity.x = move_toward(velocity.x, 0, move_speed)
			velocity.z = move_toward(velocity.z, 0, move_speed)
		
		move_and_slide()
