extends Area2D
class_name Pet

enum Mood {
	CONTENT,
	HAPPY,
	SAD,
	ANGRY
	}

var mood: Mood = Mood.CONTENT

# Symptom bars: 0.0 - 1.0 max.
var fatigue: float = 0.0
var hunger: float = 0.0
var thirst: float = 0.0
var boredom: float = 0.0
var illness: float = 0.0

# Seconds before reaching the 1.0 max.
var fatigue_rate: float = 10
var hunger_rate: float = 3
var thirst_rate: float = 15
var boredom_rate: float = 8
var illness_rate: float = 20
