# Cosmic Pets

A computer summer camp video game project.


## Gameplay

### Pets

Each pet has a set of indicator bars for:

- Fatigue
- Hunger
- Friskiness


### Rooms & Furniture

3D rooms & furniture?

- Players use the mouse to drag and place items around a room.


## Cosmic Theme

- Spaceship rooms.
- Cosmic furniture.
- [ ] Design a color palette to work from.
- [ ] Document the world units and measurements.


## Dev Tools

- Godot - https://godotengine.org/
- Krita - https://krita.org/
- Blender - https://www.blender.org/
- Bespoke - https://www.bespokesynth.com/
- Ardour - https://ardour.org/
- PureRef - https://www.pureref.com/